package com.example.demo.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@Entity
@EntityScan
@Table(name = "cars")
public class Car {

	@Column(nullable = true)
	private String buyer_name;

	@Column(nullable = true)
	private Date date_of_sale;

	@Column(nullable = true)
	private Integer final_price;

	private String name;

	private int num_of_seats;

	private int price;

	@Id
	private int id;

	public Car(String buyer_name,Date date_of_sale,Integer final_price,String name,int num_of_seats,int price) {
		super();
		this.name = name;
		this.price = price;
		this.num_of_seats = num_of_seats;
		this.date_of_sale = date_of_sale;
		this.final_price = final_price;
		this.buyer_name = buyer_name;
	}

	public Car() {
	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getNum_of_seats() {
		return num_of_seats;
	}

	public void setNum_of_seats(int num_of_seats) {
		this.num_of_seats = num_of_seats;
	}

	public Date getDate_of_sale() {
		return date_of_sale;
	}

	public void setDate_of_sale(Date date_of_sale) {
		this.date_of_sale = date_of_sale;
	}

	public Integer getFinal_price() {
		return final_price;
	}

	public void setFinal_price(Integer final_price) {
		this.final_price = final_price;
	}

	public String getBuyer_name() {
		return buyer_name;
	}

	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}

}
