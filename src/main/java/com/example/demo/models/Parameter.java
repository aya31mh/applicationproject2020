package com.example.demo.models;

import javax.persistence.Table;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@EntityScan
@Table(name = "parameters")
public class Parameter {

	@Id
	private int id;
	@Column(name = "parameter_key")
	private String key;
	private int value;
	
	
	
	
	public Parameter() {}
	public Parameter(String key, int value) {
		super();
		this.key = key;
		this.value = value;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Parameter [id=" + id + ", key=" + key + ", value=" + value + "]";
	}
	
	
	
	
}
