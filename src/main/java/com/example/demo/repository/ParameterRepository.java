package com.example.demo.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.Parameter;

@Repository
public interface ParameterRepository extends CrudRepository<Parameter, Integer> {

//	@Cacheable("parameter")
	public Parameter findByKey(String key);
	
	@Cacheable("parameter")
	public Parameter findById(int id);
}
