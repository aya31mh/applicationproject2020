package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.models.Car;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends CrudRepository<Car, Integer>{
	
	public Car findById(int id);
	public Car findByName(String name);

}
