package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.Car;
import com.example.demo.models.Parameter;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.ParameterRepository;

@Service
@Transactional
public class CarService {

	private final CarRepository carRepository;
	
	@Autowired
	public ParameterRepository parameterRepository;
	 

	public CarService(CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	public Car addCar(Car car) { 
		if (car.getNum_of_seats() == 0)
			{ 
			car.setNum_of_seats(parameterRepository.findByKey("number_of_seats").getValue());
			}  
		car.setFinal_price(car.getPrice()+(car.getPrice()*parameterRepository.findByKey("profit_ratio").getValue())/100);

		return carRepository.save(car);
	}
	
	

	public List<Car> showListCar() {
		List<Car> cars = new ArrayList<Car>();
		for (Car car : carRepository.findAll()) {

			if (car.getBuyer_name() == null) {
				System.out.println(car.getBuyer_name());
				cars.add(car);
			}
		}
		return cars;
	}

	public void deleteCar(int id) {
		carRepository.deleteById(id);
	}

	public Car editCarInfo(int id) {
		Car car= carRepository.findById(id); 
		return car;
	}
	
	public Car getCarById(int id) {
		return carRepository.findById(id);
	}

	public Car sellCar(int id) {
		return carRepository.findById(id);
	}

	public Car findByName(String name) {
		 
		return carRepository.findByName(name);
	}

}
