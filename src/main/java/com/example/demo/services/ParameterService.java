package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager; 
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.demo.models.Parameter;
import com.example.demo.repository.ParameterRepository;

@Service
@Transactional
public class ParameterService {

    public final ParameterRepository parameterRepository;

    @Autowired
    public  CacheManager cacheManager;

    public ParameterService(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public void addParameter(Parameter parameter) {
        parameterRepository.save(parameter);
    }

    @Cacheable("parameter")
    public Parameter get(int id) {
        simulateSlowService();
        return parameterRepository.findById(id);
    }

    @Cacheable("parameter")
    public List<Parameter> showListParameter() {
		Cache cache= cacheManager.getCache("parameter");
		System.out.println("cache = "+cache.getNativeCache().toString());
        List<Parameter> parameters = new ArrayList<Parameter>();
        for (Parameter parameter : parameterRepository.findAll()) {
            parameters.add(parameter);

        }

        return parameters;
    }


    public int getValueByKey(String key) {
        return parameterRepository.findByKey(key).getValue();
    }


    @Cacheable("parameter")
    @CachePut(value = "parameter")
    public Parameter update(int id) {
        return parameterRepository.findById(id);
    }

    @CacheEvict(value = "parameter", key = "#id")
    public void delete(int id) {
        parameterRepository.deleteById(id);
    }

    private void simulateSlowService() {
        try {
            long time = 3000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

}
