package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;



@SpringBootApplication
@EnableCaching
public class Demo2Application {

	public static void main(String[] args) {


		SpringApplication.run(Demo2Application.class, args);

	}

	@Bean
	public CacheManager cacheManager() {
		return new ConcurrentMapCacheManager("parameter");
	}
}
