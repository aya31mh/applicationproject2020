package com.example.demo.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.models.Car;
import com.example.demo.models.Parameter;
import com.example.demo.models.User;
import com.example.demo.services.CarService;
import com.example.demo.services.ParameterService;
import com.example.demo.services.UserService;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

@Controller
public class ApplicationController {

	@Autowired
	UserService userService;

	@Autowired
	CarService carService;

	@Autowired
	ParameterService parameterService;

	@RequestMapping("/welcome")
	public String Welcome(HttpServletRequest request) {
		request.setAttribute("cars", carService.showListCar());
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	// >>>>>>>>>>>>>>> USERS <<<<<<<<<<<<<<<<<<<<<<>

	@RequestMapping("/register")
	public String registration(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_REGISTER");
		return "welcomepage";
	}

	@PostMapping("/save-user")
	public String registerUser(@ModelAttribute User user, BindingResult bindingResult, HttpServletRequest request) {
		userService.saveUser(user);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@GetMapping("/show-users")
	public String showAllUsers(HttpServletRequest request) {
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "welcomepage";
	}

	@RequestMapping("/delete-user")
	public String deleteUser(@RequestParam int id, HttpServletRequest request) {
		userService.deleteMyUser(id);
		request.setAttribute("users", userService.showAllUsers());
		request.setAttribute("mode", "ALL_USERS");
		return "welcomepage";
	}

	@RequestMapping("/edit-user")
	public String editUser(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("user", userService.editUser(id));
		request.setAttribute("mode", "MODE_UPDATE");
		return "welcomepage";
	}

	@RequestMapping("/login")
	public String login(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_LOGIN");
		return "welcomepage";
	}

	@RequestMapping("/login-user")
	public String loginUser(@ModelAttribute User user, HttpServletRequest request) {
		if (userService.findByUsernameAndPassword(user.getUsername(), user.getPassword()) != null) {
			return "welcomepage";
		} else {
			request.setAttribute("error", "Invalid Username or Password");
			request.setAttribute("mode", "MODE_LOGIN");
			return "welcomepage";

		}
	}

	// >>>>>>>>>>>>>>> CARS <<<<<<<<<<<<<<<<<<<<

	@RequestMapping(value = "/add_car_info")
	public String addCarInfo(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_ADD_CAR");
		return "welcomepage";
	}

	@PostMapping("/save-car")
	public String addCar(@ModelAttribute Car car, BindingResult bindingResult, HttpServletRequest request)
			throws UnsupportedEncodingException {
		carService.addCar(car);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@PostMapping("/save-cars")
	public String addCars(@RequestBody Car car, BindingResult bindingResult, HttpServletRequest request)
			throws UnsupportedEncodingException {

		System.out.println("CCCCCCCCCCCCCCCC" + request.getContentType());
		carService.addCar(car);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@GetMapping("/show-cars")
	public String showCars(HttpServletRequest request,HttpServletResponse response) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		List<Car> cars = new ArrayList<Car>();
		cars =  carService.showListCar();
		request.setAttribute("cars", cars);
		request.setAttribute("mode", "ALL_CARS");
		
		return "welcomepage"; 
	}

	@RequestMapping(method = RequestMethod.GET,value = "/delete-car")
	public String deleteCar(@RequestParam int id, HttpServletRequest request) {
		carService.deleteCar(id);
		request.setAttribute("cars", carService.showListCar());
		request.setAttribute("mode", "ALL_CARS");
		return "welcomepage";
	}

	@RequestMapping(method = RequestMethod.GET,value = "/edit-car")
	public String editCar(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("car", carService.editCarInfo(id));
		request.setAttribute("mode", "MODE_UPDATE_CAR");
		return "welcomepage";
	}

	@RequestMapping("/sale-car")
	public String saleCar(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("car", carService.editCarInfo(id));
		request.setAttribute("mode", "MODE_SALE_CAR");
		return "welcomepage";
	}

	public void exportCSV(HttpServletResponse response) {
//
//		 //set file name and content type
//       String filename = "cars.csv";
//
//       response.setContentType("text/csv");
//       response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
//               "attachment; filename=\"" + filename + "\"");
//
//       //create a csv writer
//       StatefulBeanToCsv<Car> writer = new StatefulBeanToCsvBuilder<Car>(response.getWriter())
//               .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//               .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
//               .withOrderedResults(false)
//               .build();
//
//       //write all users to csv file
//       writer.write(cars);

		
		
		
	}
	// >>>>>>>>>>>>>> PARAMETERS <<<<<<<<<<<<<<<<<<<

	@RequestMapping("/add-parameter")
	public String addParameter(HttpServletRequest request) {
		request.setAttribute("mode", "MODE_ADD_PARAMETER");
		return "welcomepage";
	}

	@RequestMapping("/save-parameter")
	public String saveParameter(@ModelAttribute Parameter parameter, BindingResult bindingResult,
			HttpServletRequest request) {
		parameterService.addParameter(parameter);
		request.setAttribute("mode", "MODE_HOME");
		return "welcomepage";
	}

	@RequestMapping("/show-parameters")
	public String showParameters(HttpServletRequest request) {
		request.setAttribute("parameters", parameterService.showListParameter());
		request.setAttribute("mode", "ALL_PARAMETERS");
		return "welcomepage";
	}

	@RequestMapping("/delete-parameter")
	public String deleteParameter(@RequestParam int id, HttpServletRequest request) {
		parameterService.delete(id);
		request.setAttribute("parameters", parameterService.showListParameter());
		request.setAttribute("mode", "ALL_PARAMETERS");
		return "welcomepage";
	}

	@RequestMapping("/edit-parameter")
	public String editParameter(@RequestParam int id, HttpServletRequest request) {
		request.setAttribute("parameter", parameterService.update(id));
		request.setAttribute("mode", "MODE_UPDATE_PARAMETER");
		return "welcomepage";
	}

}
