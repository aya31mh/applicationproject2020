<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Spring-Project</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

<script type="text/javascript">
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	function filterFunction() {
		var input, filter, ul, li, a, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");
		for (i = 0; i < a.length; i++) {
			txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}
</script>

</head>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/welcome" class="navbar-brand">Spring-Project</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/login">Login</a></li>
					<li><a href="/register">New Registration</a></li>
					<li><a href="/show-users">All Users</a></li>
					<li><a href="/show-cars">All Cars</a></li>
					<li><a href="/add_car_info">Add Car</a></li>
					<li><a href="/add-parameter">Add Parameter</a></li>
					<li><a href="/show-parameters">All Parameters</a></li>
				</ul>
			</div>
		</div>
	</div>

	<c:choose>
		<c:when test="${mode=='MODE_HOME' }">
			<div class="container " id="homediv">
				<div class="jumbotron text-center">
					<h1>Welcome to Spring-Project</h1>
					<br> <br>




					<div class="dropdown text-center">
						<a class="btn btn-secondary dropdown-toggle " role="button"
							onclick="myFunction()" id="dropdownMenuLink"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Select Car </a>

						<div id="myDropdown"
							class="dropdown-menu my-menu dropdown-content"
							aria-labelledby="dropdownMenuButton">
							<input type="text" placeholder="Search.." id="myInput"
								onkeyup="filterFunction()">
							<c:forEach var="car" items="${cars}">
								<a class="dropdown-item" href="/sale-car?id=${car.id }">${car.name}</a>
								<br>
							</c:forEach>

						</div>
					</div>


				</div>

			</div>
		</c:when>


		<c:when test="${mode=='MODE_REGISTER' }">
			<div class="container text-center">
				<h3>New Registration</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-user">
					<input type="hidden" name="id" value="${user.id}" />
					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username"
								value="${user.username}" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="password"
								value="${user.password}" />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Register" />
					</div>
				</form>

			</div>
		</c:when>

		<c:when test="${mode=='ALL_USERS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All Users</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>UserName</th>
								<th>Delete</th>
								<th>Edit</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="user" items="${users }">
								<tr>
									<td>${user.id}</td>
									<td>${user.username}</td>
									<td><a href="/delete-user?id=${user.id }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-user?id=${user.id }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode=='MODE_UPDATE' }">
			<div class="container text-center">
				<h3>Update User</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-user">
					<input type="hidden" name="id" value="${user.id }" />
					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username"
								value="${user.username }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="password"
								value="${user.password }" />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${mode=='MODE_LOGIN' }">
			<div class="container text-center">
				<h3>User Login</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="/login-user">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							<c:out value="${error }"></c:out>
						</div>
					</c:if>
					<div class="form-group">
						<label class="control-label col-md-3">Username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username"
								value="${user.username }" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Password</label>
						<div class="col-md-7">
							<input type="password" class="form-control" name="password"
								value="${user.password }" />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Login" />
					</div>
				</form>
			</div>
		</c:when>


		<c:when test="${mode=='ALL_CARS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All CARS</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Price</th>
								<th>Number of seats</th>
								<th>Delete</th>
								<th>Edit</th>
								<th>Sale</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="car" items="${cars }">
								<tr>
									<td>${car.id}</td>
									<td>${car.name}</td>
									<td>${car.price}</td>
									<td>${car.num_of_seats}</td>
									<td><a href="/delete-car?id=${car.id }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-car?id=${car.id }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="/sale-car?id=${car.id }"><span
											class="glyphicon glyphicon-send"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode=='MODE_SALE_CAR' }">
			<div class="container text-center">
				<h3>Sale Car</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-car">
					<input type="hidden" name="id" value="${car.id }" />

					<div class="form-group">
						<label class="control-label col-md-3">Car Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="name"
								readonly="readonly" value="${car.name }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Price</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="price"
								readonly="readonly" value="${car.price }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Number Of Seats</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="num_of_seats"
								readonly="readonly" value="${car.num_of_seats }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Date</label>
						<div class="col-md-7">
							<input type="date" class="form-control" name="date_of_sale"
								value="${car.date_of_sale }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Final Price</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="final_price"
								value="${car.final_price }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Buyer Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="buyer_name"
								value="${car.buyer_name }" />
						</div>
					</div>

					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>
		<c:when test="${mode=='MODE_ADD_CAR' }">
			<div class="container text-center">
				<h3>New Car</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-car"  >
					<input type="hidden" name="id" value="${car.id }" />

					<div class="form-group">
						<label class="control-label col-md-3">Car Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="name"
								value="${car.name }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Price</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="price"
								value="${car.price }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Number Of Seats</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="num_of_seats"
								value="${car.num_of_seats }" />
						</div>
					</div>

					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>


			</div>
		</c:when>
		<c:when test="${mode=='MODE_UPDATE_CAR' }">
			<div class="container text-center">
				<h3>Update Car Info</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-car">
					<input type="hidden" name="id" value="${car.id }" />

					<div class="form-group">
						<label class="control-label col-md-3">Car Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="name"
								value="${car.name }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Price</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="price"
								value="${car.price }" />
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">Number Of Seats</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="num_of_seats"
								value="${car.num_of_seats }" />
						</div>
					</div>

					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${mode=='MODE_ADD_PARAMETER' }">
			<div class="container text-center">
				<h3>New Parameter</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-parameter">
					<input type="hidden" name="id" value="${parameter.id }" />

					<div class="form-group">
						<label class="control-label col-md-3">Parameter Key</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="parameter_key"
								value="${parameter.parameter_key }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Value</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="value"
								value="${parameter.value }" />
						</div>
					</div>


					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Save" />
					</div>
				</form>


			</div>
		</c:when>

		<c:when test="${mode=='ALL_PARAMETERS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All Parameters</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>Key</th>
								<th>Value</th>
								<th>Delete</th>
								<th>Edit</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="parameter" items="${parameters }">
								<tr>
									<td>${parameter.id}</td>
									<td>${parameter.key}</td>
									<td>${parameter.value}</td>
									<td><a href="/delete-parameter?id=${parameter.id }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-parameter?id=${parameter.id }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

	<c:when test="${mode=='MODE_UPDATE_PARAMETER' }">
			<div class="container text-center">
				<h3>Update Parameter Info</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-parameter">
					<input type="hidden" name="id" value="${parameter.id }" />

					<div class="form-group">
						<label class="control-label col-md-3">Parameter Key</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="key"
								value="${parameter.key }" />
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3">Value</label>
						<div class="col-md-7">
							<input type="number" class="form-control" name="value"
								value="${parameter.value }" />
						</div>
					</div>


					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>

	</c:choose>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>

