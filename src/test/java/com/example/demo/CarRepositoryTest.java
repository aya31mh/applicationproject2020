package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.sql.MckoiCaseFragment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.models.Car;
import com.example.demo.models.Parameter;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.ParameterRepository;
import com.example.demo.services.CarService;
import com.example.demo.services.ParameterService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CarRepositoryTest {
//
//	@InjectMocks
//	private CarService carService;
//
//	@Mock
//	private CarRepository carRepository;
//
//	@InjectMocks
//	private ParameterService parameterService;
//
//  
//	@Mock
//	private ParameterRepository parameterRepository;
//	
//	
//	List<Car> cars = new ArrayList<Car>();
//	Parameter parameter2;
//	Parameter parameter1;
//
//	@BeforeEach
//	public void setup() {
//
//		// given
//
//		ObjectMapper mapper = new ObjectMapper();
//
//		TypeReference<List<Car>> typeReference = new TypeReference<List<Car>>() {
//		};
//		InputStream inputStream = TypeReference.class.getResourceAsStream("/json/data.json");
//
//		try {
//			cars = mapper.readValue(inputStream, typeReference);
//
//			System.out.println("cars Saved!");
//		} catch (IOException e) {
//			System.out.println("Unable to save cars: " + e.getMessage());
//		}
//
//		parameter1 = new Parameter("number_of_seats", 4);
//	
//		parameter2 = new Parameter("profit_ratio", 10);
//		
//
//	
//		for (int i = 0; i < cars.size(); i++) {
//			Mockito.when(parameterRepository.findByKey("number_of_seats")).thenReturn(parameter1);
//			Mockito.when(parameterRepository.findByKey("profit_ratio")).thenReturn(parameter2);
//
//			
//			cars.get(i).setNum_of_seats(parameterService.getValueByKey("number_of_seats"));
//			
//			Mockito.when(carRepository.findByName(cars.get(i).getName())).thenReturn(cars.get(i));
//			Mockito.when(carRepository.findById(cars.get(i).getId())).thenReturn(cars.get(i));
////			Mockito.when(carService.editCarInfo(cars.get(i).getId())).thenReturn(cars.get(i));
//			Mockito.when(carRepository.save(cars.get(i))).thenReturn(cars.get(i));
//
//			
//		}
//
//	}
//
//	@Test
//	public void whenFindByName_thenReturnCar() {
//
//		// when
//		Car found = carService.findByName("Mazda");
//
//		// then
//		assertThat(found.getName()).isEqualTo("Mazda");
//	}
//
//	@Test
//	public void whenFindById_thenEdit() {
//
//		// when
//		
////		Car car = carRepository.findById(6);
////		car.setName("aya"); 
////		carRepository.save(car);
////		Mockito.when(carRepository.findByName("aya")).thenReturn(car);
////		Car car2 = carRepository.findByName("aya");  
////		assertThat(car2.getName()).isEqualTo("aya");
//		
//		Car car = carService.editCarInfo(6);
//		System.out.println("LL "+car.getName());
//		car.setName("KKKK");
//		carService.addCar(car);
//		
//		
//	}
//	
//	@Test
//	public void whenAddCar_thenReturnCarName() {
//		
//		//when
//		Car car = new Car("saba",5000000,0,null,null,null);
//		carRepository.save(car);
//		Mockito.when(carRepository.findByName("saba")).thenReturn(car);
//		Car found = carRepository.findByName("saba"); 
//		assertThat(found.getPrice()).isEqualTo(5000000);
//	}
//	
//	@Test
//	public void whenDeleteCar_thenReturn() { 
//		carRepository.deleteById(6);
//		
//		Car found = carRepository.findById(6);
//		 
//		assertThat(found).isEqualTo(null);
//	}
//	
//	

}
