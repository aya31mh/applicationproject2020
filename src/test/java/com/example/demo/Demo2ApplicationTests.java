package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest; 
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controller.ApplicationController;
import com.example.demo.models.Car;
import com.example.demo.repository.CarRepository;

@SpringBootTest
class Demo2ApplicationTests {

	@Autowired
	private ApplicationController controller;
	
	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
