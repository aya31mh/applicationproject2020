package com.example.demo;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import com.example.demo.controller.ApplicationController;
import com.example.demo.models.Car;
import com.example.demo.models.Parameter;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.ParameterRepository;
import com.example.demo.services.CarService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)  
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = Demo2Application.class)
@AutoConfigureMockMvc  
@TestPropertySource(locations = "classpath:application-test.properties")

public class teeest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ParameterRepository parameterRepository;

	@Autowired
	private CarService carService;
	
	
	@BeforeEach
	public void setup() {
		Parameter parameter1 = new Parameter("number_of_seats", 4);

		Parameter parameter2 = new Parameter("profit_ratio", 10);

		Mockito.when(parameterRepository.findByKey("number_of_seats")).thenReturn(parameter1);
		Mockito.when(parameterRepository.findByKey("profit_ratio")).thenReturn(parameter2);

		ObjectMapper mapper = new ObjectMapper();
		
				TypeReference<List<Car>> typeReference = new TypeReference<List<Car>>() {
				};
				InputStream inputStream = TypeReference.class.getResourceAsStream("/json/data.json");
		
				try {
					List<Car> cars = mapper.readValue(inputStream, typeReference);
					for (int i = 0; i < cars.size(); i++) {
						carService.addCar(cars.get(i));
					} 
					
					System.out.println("cars Saved!");
				} catch (IOException e) {
					System.out.println("Unable to save cars: " + e.getMessage());
				}
		
		
	}
  
	
	public void exportDataToCsvFile() {
	 
	}
	

	@Test
	public void getCars_Test() throws Exception {

		mockMvc.perform(get("/show-cars")).andExpect(status().isOk()).andExpect(view().name("welcomepage"))
				.andExpect(request().attribute("cars", hasSize(100))) ;

	}

	@Test
	public void AddCar_Test() throws Exception {

		Car car = new Car(null, null, null, "tbbb", 0, 333333);
		ObjectMapper oM = new ObjectMapper();

		mockMvc.perform(post("/save-cars").content(oM.writeValueAsString(car)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(view().name("welcomepage"))
				.andExpect(request().attribute("mode", is("MODE_HOME")));

	}

	@Test
	public void EditCar_test() throws JsonProcessingException, Exception {

		mockMvc.perform(get("/edit-car").param("id", "7")).andExpect(status().isOk())
				.andExpect(view().name("welcomepage")).andExpect(request().attribute("mode", "MODE_UPDATE_CAR"));
	}

	@Test
	public void deleteCar_test() throws JsonProcessingException, Exception {

		mockMvc.perform(get("/delete-car").param("id", "6")).andExpect(status().isOk())
				.andExpect(view().name("welcomepage")).andExpect(request().attribute("mode", "ALL_CARS"));
	}
	
//	@After
//	public void dorpData() {
//		List<Car> cars=new ArrayList<Car>();
//		cars=carService.showListCar();
//		
//		for (int i = 0; i < cars.size(); i++) {
//			carService.deleteCar(cars.get(i).getId());
//		}
//	}

}
